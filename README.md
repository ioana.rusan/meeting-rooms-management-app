# Shared Meeting Room Reservation Management System

_**- WEB APPLICATION:**_
    
**Prerequisites**
1. Node.js: You can download and install Node.js from the official website: https://nodejs.org
1. npm (Node Package Manager): npm is typically installed along with Node.js.

**Installation**
1. Open a terminal or command prompt and navigate to the project's root directory.
1. Install project dependencies by running the following command:`npm install
`
This command will read the package.json file and download all the required dependencies specified in it.

**Starting the App**

Once you have installed the dependencies and configured the necessary settings, you are ready to start the React web app. Follow these steps:

1. In the terminal or command prompt, navigate to the project's root directory if you are not already there.
1. Run the following command to start the development server:`npm start
`
This command will start the development server and compile the React app. Once the compilation process is complete, you should see a message indicating that the app is running.

1. Open your web browser and visit http://localhost:3000. You should see the React web app running.


_**- BACKEND SOLUTION**_

**Prerequisites**

1. Visual Studio 2022: You can download and install Visual Studio 2022 from the official website: https://visualstudio.microsoft.com/downloads

1. NET 7 SDK: You can download and install the .NET 7 SDK from the official website: https://dotnet.microsoft.com/download/dotnet/7.0

**Installation**

1. Open Visual Studio 2022.
1. Click on "Open a project or solution" on the start page, or go to "File" > "Open" > "Project/Solution" in the menu bar.
1. Navigate to the folder where you cloned or downloaded the backend solution, and select the solution file (.sln) to open the project in Visual Studio.
1. Once the solution is opened, Visual Studio will prompt you to install any missing packages or SDKs required by the project. Follow the prompts to install the necessary dependencies.

_Side Note_: The backend solution is not need to be run for using the server side of the application because it was deployed using Azure DevOps and all the APIs links are setted to the ones that are deployed. 

_**- MOBILE APPLICATION:**_

**Prerequisites**
1. Node.js: You can download and install Node.js from the official website: https://nodejs.org
1. Expo CLI: Install the Expo CLI globally by running the following command in the terminal:`npm install -g expo-cli
`
1. Xcode: Xcode is required for iOS development. You can install Xcode from the Mac App Store or from the Apple Developer website. 
1. iOS table device, preferably IPad 9th generation.

**Installation**
1. Open a terminal or command prompt and navigate to the project's root directory.
1. Install project dependencies by running the following command:`npm install
`
This command will read the package.json file and download all the required dependencies specified in it.

**Starting the App**

Once you have installed the dependencies and configured the necessary settings, you are ready to start the React web app. Follow these steps:
Make sure you have Xcode installed and set up on your macOS machine.

1. In the terminal, navigate to the project's root directory if you are not already there.
1. Start the Expo development server by running the following command:
`expo start
`
1. This command will start the Expo development server and compile the React Native app. Once the compilation process is complete, you should see a QR code in the terminal and a web page with development options.
1. Install the Expo Go app on your iOS device from the App Store.
1. Scan the QR code displayed in the terminal using the Camera app on your iOS device. Alternatively, you can open the Expo Go app and use the "Scan QR Code" option.
1. Once the QR code is scanned, the Expo Go app will open and load your React Native app. You can interact with the app on your iOS device.

